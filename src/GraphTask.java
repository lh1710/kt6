import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }


   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      Graph g = new Graph("G");

      //Find shortest path from one point to another
      g.createRandomGraph(4,4,3);
      g.displayGraph();
      g.shortestPathsFrom(g.first);
      System.out.println("Shortest path");
      g.displayShortestBackTrackPath();
      System.out.println("\n");

      // Check if graph has Euler's cycle and if it has, print it out.
      g.findpath(g.connectedMatrix, g.connectedMatrix.length);
      System.out.println("\n");

      // Given a vertex, find a vertex that is furthest from the given vertex.
      ArrayList<Vertex> l = g.first.getNextVertexList();

      Random rand = new Random();

      Vertex v = (g.first.getNextVertexList().get(rand.nextInt(l.size())));
      System.out.println(String.format("Furthest elements form %s are: %s ", v ,g.getFurthestVertexFromGivenVertex(v)));
      System.out.println("\n");

      // Check if graph has Euler's path and if it has, print it out.
      g.countTips();
      if (g.hasEulerPath()) {
         System.out.println("Graph has Euler's path");
         System.out.println(g.enumerateEulerPath());
      }
   }

   /**
    * Graphs are objects that contain all the Vertices and Arcs needed
    * to represent all the possible paths between different destinations.
    * All the methods for finding the shortest path and displaying it are
    * contained in the
    * Graph class.
    */
   class Graph {

      private final String id;
      private Vertex first;
      private Vertex finish;
      private Vertex[] vertexArray;
      private int info = 0;

      private Map<String, ArrayList<Arc>> mapOfArcs = new HashMap<>();
      private ArrayList<String> vertexes = new ArrayList<>();
      private ArrayList<Arc> arcs = new ArrayList<>();

      private int[][] connectedMatrix;
      private List<Arc> listOfAllArcs = new ArrayList<>();
      private List<String> listOfArcsAsStrings = new ArrayList<>();
      private List<Arc> EulersArcs = new ArrayList<>();


      Graph(String id, Vertex first, Vertex finish) {
         this.id = id;
         this.first = first;
         this.finish = finish;
      }

      Graph(String s) {
         this(s, null, null);
      }

      /**
       * Method for creating a string representation of the Graph.
       */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(a);
               a = a.next;
            }
            sb.append (nl);
            v = v.previous;
         }
         return sb.toString();
      }

      /**
       * Method for creating a Vertex.
       */
      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.previous = first;
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Method for creating an weighed arc.
       *
       * @param aid name of the arc
       * @param from Vertex from where the Arc starts
       * @param to Vertex where the Arc ends
       * @param length the weight of the Arc
       */
      public Arc createArc(String aid, Vertex from, Vertex to, int length) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         res.length = length;

         listOfAllArcs.add(res);
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n, int maxLength) {

         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];

         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));

            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               int length = (int) (Math.random() * maxLength);

               Arc a = createArc(varray[vnr].toString(), varray[vnr],
                       varray[i], length);
               varray[vnr].addArc(a);

               Arc b = createArc(varray[i].toString(), varray[i],
                       varray[vnr], length);
               varray[i].addArc(b);
            }
         }

         if (finish == null) {
            finish = varray[0];
         }
         if (first == null) {
            first = varray[n-1];
         }

         vertexArray = varray;
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         int info = 0;
         Vertex v = first;
         while (v != null) {
            v.vDistFromStart = info++;
            v = v.previous;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.vDistFromStart;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.vDistFromStart;
               res[i][j]++;
               a = a.next;
            }
            v = v.previous;
         }
         return res;
      }

      public int[][] createAdjMatrix2() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      public void createRandomGraph(int vNum, int eNum, int maxLen) {
         if (vNum <= 0)
            return;
         if (vNum > 2500)
            throw new IllegalArgumentException("Too many vertices: " + vNum);
         if (eNum < vNum - 1 || eNum > vNum * (vNum - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + eNum);
         first = null;
         createRandomTree(vNum, maxLen); // n-1 edges created here
         Vertex[] vert = new Vertex[vNum];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.previous;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = eNum - vNum + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * vNum);  // random start
            int j = (int) (Math.random() * vNum);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];

            int randomWeight = (int) (Math.random() * maxLen);

            Arc a = createArc(vi.toString(), vi, vj, randomWeight);
            connected[i][j] = 1;
            vi.addArc(a);

            Arc b = createArc(vj.toString(), vj, vi, randomWeight);
            connected[j][i] = 1;
            vj.addArc(b);

            edgeCount--;  // a new edge happily created
         }
         connectedMatrix = connected;
      }

      /**
       * KADRI OSA!!!
       * A method for reversing a list.
       *
       * @param alist a list of Arcs that needs to be reversed
       */
      public ArrayList<Arc> reverseArrayList(ArrayList<Arc> alist)
      {
         ArrayList<Arc> revArrayList = new ArrayList<>();
         for (int i = alist.size() - 1; i >= 0; i--) {

            revArrayList.add(alist.get(i));
         }
         return revArrayList;
      }

      /**
       * Method for checking if the Vertex is already included in the Vertex array.
       */
      public boolean contains(Vertex v) {
         for (Vertex each : vertexArray
         ) {
            if (each.id.equals(v.id)) {
               return true;
            }
         }
         return false;
      }

      /**
       * A help method for displaying a manually created Graph.
       * Displays Vertices and all the Arcs that derive from them.
       */
      public void displayGraph() {
         for (Vertex v : vertexArray) {
            StringBuilder sb = new StringBuilder();
            sb.append(v);
            sb.append(" --> ");
            for (Arc a : v.arcs) {
               sb.append(a);
            }
            System.out.println(sb);
         }
      }

      /**
       * Shortest paths from a given vertex. Uses Dijkstra's algorithm.
       * For each vertex vInfo is length of shortest path from given
       * start s and vObject is previous vertex from s to this vertex.
       *
       * @param s start vertex
       */
      public void shortestPathsFrom(Vertex s) {
         if (vertexArray == null) return;
         if ((!contains(s)))
            throw new RuntimeException("wrong argument to Dijkstra!!");
         int INFINITY = Integer.MAX_VALUE / 4; // big enough!!!

         for (Vertex v : vertexArray) {
            v.vDistFromStart = INFINITY;
            v.previous = null;
         }

         s.vDistFromStart = 0;
         List vq = Collections.synchronizedList(new LinkedList());
         vq.add(s);
         while (vq.size() > 0) {
            int minLen = INFINITY;
            Vertex minVert = null;
            for (Object vertex : vq) {
               Vertex v = (Vertex) vertex;
               if (v.vDistFromStart < minLen) {
                  minVert = v;
                  minLen = v.vDistFromStart;
               }
            }

            if (minVert == null)
               throw new RuntimeException("error in Dijkstra!!");
            if (vq.remove(minVert)) {
               // minimal element removed from vq
            } else
               throw new RuntimeException("error in Dijkstra!!");

            for (Arc arc : minVert.arcs) {
               int newLen = minLen + arc.length;
               Vertex to = arc.target;
               if (to.vDistFromStart == INFINITY) {
                  to.backTrackArc = arc;
                  vq.add(to);
               }
               if (newLen < to.vDistFromStart) {
                  to.vDistFromStart = newLen;
                  to.previous = minVert;
                  to.backTrackArc = arc;
               }
            }
         }
      } // end of shortestPathsFrom()

      /**
       * A method for displaying the Graph after the shortest path has been found.
       * Displays a list of weighed Arcs from the starting point to the finish.
       */
      public void displayShortestBackTrackPath() {

         ArrayList<Arc> path = new ArrayList<>();

         if (first == null || finish == null) {
            System.out.println("Graph " + id + " is empty");
            return;
         }

         Vertex v = finish;

         for (int i = 0; i < vertexArray.length; i++) {
            path.add(v.backTrackArc);
            if (v.previous.equals(first)) {
               break;
            }
            v = v.previous;
         }

         path = reverseArrayList(path);

         System.out.println(path);

      }

      /**
       * LIISA OSA!!!
       * counts the arcs and vertexes and puts them to a list
       */
      public void countTips() {
         Vertex v = this.first;
         while (v != null) {
            ArrayList<Arc> x = new ArrayList<>();
            Arc a = v.first;
            while (a != null) {
               this.arcs.add(a);
               this.vertexes.add(v.toString());
               this.vertexes.add(a.target.toString());
               x.add(a);
               a = a.next;
            }
            this.mapOfArcs.put(v.toString(), x);
            v = v.next;
         }
      }

      /**
       * creates string array from Arraylist
       */
      public String[] getStringArray(ArrayList<String> arr)
      {
         Object[] objArr = arr.toArray();

         String[] str = Arrays
                 .copyOf(objArr, objArr
                                 .length,
                         String[].class);
         return str;
      }

      /**
       * checks if graph has euler path
       */
      public boolean hasEulerPath() {
         int uneven = 0;
         Map<String, Integer> countedTips = this.makeMap(this.getStringArray(this.vertexes));
         if (countedTips.size() == 0) {
            throw new RuntimeException("Please create a valid graph with valid verteces");
         }
         for (Map.Entry<String, Integer> entry : countedTips.entrySet()) {
            if (entry.getValue() % 2 == 1) {
               uneven += 1;
            }
         }
         return uneven == 2 || uneven == 0;
      }

      /**
       * creates a map with the vertex as key and the count of its arcs as value
       */
      public Map<String, Integer> makeMap(String[] array) {
         Map<String, Integer> countedTips = new HashMap<>();
         Arrays.stream(array)
                 .collect(Collectors.groupingBy(s -> s))
                 .forEach((k, v) -> countedTips.put(k, v.size()/2));
         return countedTips;
      }


      /**
       * enumerates Euler's path and returns a list of the result
       */
      public Serializable enumerateEulerPath() {
         if (arcs.isEmpty()) {
            throw new RuntimeException("Please create a graph");
         }
         ArrayList<Arc> path = new ArrayList<>();
         if (hasEulerPath()) {
            String start = "";
            for (Map.Entry<String, ArrayList<Arc>> entry : mapOfArcs.entrySet()) {
               if (entry.getValue().size() % 2 == 1) {
                  start = entry.getKey();
                  break;
               }
            }
            if (start.equals("")) {
               start = mapOfArcs.keySet().iterator().next();
            }
            ArrayList<Arc> copy = new ArrayList<>(arcs);
            while (arcs.size() != 0) {
               ArrayList<Arc> x = new ArrayList<>();
               for (Arc element : arcs) {
                  if (element.id.replaceAll("a", "").split("_")[0].equals(start))
                     x.add(element);
               }
               for (Arc item : x) {
                  String[] y = item.toString().replace("(", "").split("-");
                  y[4] = y[4].replace(")", "");
                  y[4] = y[4].replace(">", "");
                  if (this.makeMap(getStringArray(vertexes)).get(y[4]) > 1) {
                     arcs.remove(item);
                     path.add(item);
                     for (Arc a : copy) {
                        if (a.target.id.equals(y[0]) && a.id.equals(y[4])) {
                           arcs.remove(a);
                           break;
                        }
                     }
                     start = item.target.id;
                     break;
                  }
               }
            }
         }
         else {
            return ("The graph has no euler path, therefore the verteces cannot be enumerated!");
         }
         return path;
      }

      /**
       * RASMUSE OSA!!!
       * Finds if connected graph has
       * Eulerian cycle and if it has, it numbers all the edges in order from start vertex back to itself
       * not numbering any edge twice.
       * https://www.geeksforgeeks.org/eulerian-path-undirected-graph/
       * @param graph adjacent matrix
       * @param n adjacent matrix length
       */
      public void findpath(int[][] graph, int n)
      {
         Vector<Integer> numofadj = new Vector<>();
         /*
          Find out number of edges each vertex has.
          */
         for (int i = 0; i < n; i++)
            numofadj.add(accumulate(graph[i], 0));

         /*
          Find out how many vertexes has even number edges
          */
         int startPoint = 0;
         for (int i = n - 1; i >= 0; i--)
         {
            if (numofadj.elementAt(i) % 2 == 0)
            {
               startPoint = i;
            } else {
               System.out.println("Graph does not contain Euler's cycle.");
               return;
            }
         }

         /*
          Initialize empty stack and path
          Can start path from any of the vertexes because in Eulerian cycle it does not matter where do you
          start from.
          */
         Stack<Integer> stack = new Stack<>();
         Stack<Integer> path = new Stack<>();
         int cur = startPoint;

         /*
          Loop will run until there is no element in the stack
          or current edge has some neighbour.
          */
         while (!stack.isEmpty() || accumulate(graph[cur], 0) != 0)
         {

            /*
             If current vertex has not any neighbour
             add it to path and pop stack
             set new current to the popped element.
             */
            if (accumulate(graph[cur], 0) == 0)
            {
               path.add(cur);
               cur = stack.pop();
            }
            /*
             If the current vertex has at least one
             neighbour add the current vertex to stack,
             remove the edge between them and set the
             current to its neighbour.
             */
            else
            {
               for (int i = 0; i < n; i++)
               {
                  if (graph[cur][i] == 1)
                  {
                     stack.add(cur);
                     graph[cur][i] = 0;
                     graph[i][cur] = 0;
                     cur = i;
                     break;
                  }
               }
            }
         }

         /*
         Creates arc alike strings according to Euler's path and adds them to list.
         */
         for (int i = 0; i < path.size(); i++) {
            String s = "";
            if (i == path.size() - 1) {
               s =  "v" + (path.get(i) + 1) + "v" + (path.get(0) + 1);
            } else {
               s =  "v" + (path.get(i) + 1) + "v" + (path.get(i + 1) + 1);
            }
            listOfArcsAsStrings.add(s);


         }

         System.out.println("Graph contains Euler's cycle");

         /*
         Compares listOfAllArcs where all the graphs arcs are stored in, with listOfArcsAsStrings where all the Euler's
         cycle arcs are stored at. When arcs match, arc from listOfAllArcs is added to EulersArcs list, which in the end,
         is printed out to user.
         */
         for (String arc : listOfArcsAsStrings) {
            for (Arc realarc : listOfAllArcs) {
               if (arc.equals(realarc.id + realarc.target.id)) {
                  EulersArcs.add(realarc);
               }
            }
         }
         System.out.println(EulersArcs);
      }

      /**
       * Helper method for findPath. Counts how many edges vertex has.
       * @param arr array of a adjacent matrix
       * @param sum sum of a matrix
       * @return Number of edges a vertex has
       */
      int accumulate(int[] arr, int sum)
      {
         for (int i : arr)
            sum += i;
         return sum;
      }



      /**
       * MARI-LOTTA OSA!!!
       * Create a Distance matrix of this graph.
       * <p>
       * based on https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * Chapter: Kauguste Arvutamine.
       *
       * @return distance matrix
       */
      public int[][] getDistanceMatrix() {
         int[][] adj = this.createAdjMatrix2();
         int numberOfVertices = adj.length;
         int[][] result = new int[numberOfVertices][numberOfVertices];
         if (numberOfVertices < 1) return result;
         int INFINITY = Integer.MAX_VALUE / 2; //biggest int possible
         for (int i = 0; i < numberOfVertices; i++) {
            for (int j = 0; j < numberOfVertices; j++) {
               if (adj[i][j] == 0) {
                  result[i][j] = INFINITY;
               } else {
                  result[i][j] = 1;
               }
            }
         }
         for (int i = 0; i < numberOfVertices; i++) {
            result[i][i] = 0;
         }
         return result;
      }

      /**
       * Creates a shortest paths matrix of this graph.
       * <p>
       * Based on https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * Chapter: Kauguste arvutamine, Floyd-Warshalli algoritm.
       *
       * @return Shortest paths matrix
       */
      public int[][] getShortestPathsMatrix() {
         int[][] distanceMatrix = this.getDistanceMatrix();
         int numberOfVertices = distanceMatrix.length;
         for (int k = 0; k < numberOfVertices; k++) {
            for (int i = 0; i < numberOfVertices; i++) {
               for (int j = 0; j < numberOfVertices; j++) {
                  int newlength = distanceMatrix[i][k] + distanceMatrix[k][j]; //alternative path
                  if (distanceMatrix[i][j] > newlength) {
                     distanceMatrix[i][j] = newlength; // new path is shorter
                  }
               }
            }
         }
         return distanceMatrix;
      }


      /**
       * Create an ArrayList of Vertexes that are the furthest from given Vertex of this graph.
       *
       * @param v number of vertices
       * @return ArrayList of furthest Vertexes.
       */
      public ArrayList<Vertex> getFurthestVertexFromGivenVertex(Vertex v) {
         ArrayList<Vertex> vertexes = this.first.getNextVertexList();

         if (!vertexes.contains(v)) { // Making sure that Given Vertex belongs to this Graph
            throw new RuntimeException(String.format("Given Vertex: %s is not part of this Graph!", v));
         }

         int baseVertexIndex = vertexes.indexOf(v); // row of the given vertex in matrix.
         int[][] shortestPathMatrix = this.getShortestPathsMatrix();
         int numberOfVertices = shortestPathMatrix.length;
         ArrayList<Vertex> furthestVertices = new ArrayList<>();

         // find longest path.
         int longestWay = 0;
         for (int j = 0; j < numberOfVertices; j++) {
            if (longestWay < shortestPathMatrix[baseVertexIndex][j]) {
               longestWay = shortestPathMatrix[baseVertexIndex][j];
            }
         }

         //Determine all Vertexes with longest path to v
         for (int j = 0; j < numberOfVertices; j++) {
            if (longestWay == shortestPathMatrix[baseVertexIndex][j]) {
               furthestVertices.add(vertexes.get(j));
            }
         }
         return furthestVertices;
      }
   }

   /**
    * Class Vertex for representing all the milestones along the path
    * from start to finish. Vertex class holds all the Arcs that derive
    * from it and connect it to the other vertices.
    */
   class Vertex {

      private final String id;
      private Vertex previous;
      private Arc first;
      private Arc backTrackArc;
      private int vDistFromStart = 0;
      private final ArrayList<Arc> arcs = new ArrayList<>();
      private Vertex next;
      private int info = 0;

      Vertex(String s, Vertex v, Arc e, Vertex n) {
         id = s;
         previous = v;
         first = e;
         next = n;
      }

      Vertex(String s) {
         this(s, null, null, null);
      }

      /**
       * Method for creating a string representation of the Vertex.
       */
      @Override
      public String toString() {
         return id;
      }

      /**
       * Find the amount of Vertexes attached in a chain to this Vertex
       * Includes this vertex.
       *
       * @return amount of vertexes chained to this vertex.
       */
      public int getVertexCount() {
         int vertexCount = 1;
         Vertex n = this;
         while (n != null) {
            vertexCount++;
            //System.out.println(String.format("id: %s,%s count: %s",id,n.id,vertexCount));
            n = n.next;
            //
         }
         return vertexCount;
      }

      /**
       * Method for checking if the Arc list already contains the parameter Arc.
       * @param arc Arc to be checked
       */
      public boolean containsArc(Arc arc) {
         for (Arc a : arcs) {
            if (a.id.equals(arc.id) && a.target.equals(arc.target)) {
               return true;
            }
         }
         return false;
      }

      /**
       * Method for adding an arc to the Vertex arc list.
       * @param arc Arc to be added
       */
      public void addArc(Arc arc) {
         if (!containsArc(arc)) {
            this.arcs.add(arc);
         }
      }

      public ArrayList<Vertex> getNextVertexList() {
         ArrayList<Vertex> vertexes = new ArrayList<>();
         Vertex n = this;
         while (n != null) {
            vertexes.add(n);
            //System.out.println(String.format("id: %s,%s count: %s",id,n.id,vertexCount));
            n = n.next;
            //
         }
         return vertexes;
      }
   }

   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private final String id;
      private Vertex target;
      private Arc next;
      private int length = 0;
      private int info = 0;


      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      /**
       * Method for creating a string representation of the Arc.
       */
      @Override
      public String toString() {
         StringBuilder sb;
         sb = new StringBuilder();
         sb.append("(");
         sb.append(id);
         sb.append("--");
         sb.append(length);
         sb.append("-->");
         sb.append(target);
         sb.append(")");

         return sb.toString();
      }
   }
}